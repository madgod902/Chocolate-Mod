package tyrannus.chocolate.data.client;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.FluidTagsProvider;
import net.minecraft.data.tags.TagsProvider;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.material.WaterFluid;
import net.minecraftforge.common.Tags;
import net.minecraftforge.common.data.ExistingFileHelper;
import tyrannus.chocolate.Chocolate;
import tyrannus.chocolate.setup.ModFluids;
import tyrannus.chocolate.setup.ModTags;

import javax.annotation.Nullable;

import static tyrannus.chocolate.ModLogger.MODID;

public class ModFluidTagsProvider extends FluidTagsProvider {
    public ModFluidTagsProvider(DataGenerator gen, @Nullable ExistingFileHelper existingFileHelper) {
        super(gen, MODID, existingFileHelper);
    }

        protected void addTags() {
            tag(ModTags.Fluids.CHOCOLATE_LIQUIDS)
                    .add(ModFluids.FLOWINGMELTEDCHOCOLATE.get()).add(ModFluids.MELTEDCHOCOLATE.get())
                    .add(ModFluids.FLOWINGMILKMELTEDCHOCOLATE.get()).add(ModFluids.MILKMELTEDCHOCOLATE.get())
                    .add(ModFluids.FLOWINGDARKMELTEDCHOCOLATE.get()).add(ModFluids.MELTEDDARKCHOCOLATE.get());


            tag(FluidTags.WATER)
                    .addTag(ModTags.Fluids.CHOCOLATE_LIQUIDS);
        }
}
