package tyrannus.chocolate.data;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BlockTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.forge.event.lifecycle.GatherDataEvent;
import tyrannus.chocolate.Chocolate;
import tyrannus.chocolate.data.client.*;
import tyrannus.chocolate.data.data.ModRecipeProvider;

import static tyrannus.chocolate.ModLogger.MODID;


@Mod.EventBusSubscriber(modid = MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DataGenerators {
    @SubscribeEvent
    public static void gatherData(GatherDataEvent event) {
        DataGenerator gen = event.getGenerator();
        ExistingFileHelper existingFileHelper = event.getExistingFileHelper();

        gen.addProvider(new ModBlockStateProvider(gen, existingFileHelper));
        gen.addProvider(new ModItemModelProvider(gen, existingFileHelper));
        gen.addProvider(new ModBlockTagsProvider(gen, existingFileHelper));
        //gen.addProvider(new ModItemTagsProvider(gen, existingFileHelper));
        gen.addProvider(new ModFluidTagsProvider(gen, existingFileHelper));
        gen.addProvider(new ModRecipeProvider(gen));
    }
}
