package tyrannus.chocolate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ModLogger {

    private static final Logger logManager = LogManager.getLogger();
    public static final Logger LOGGER = logManager;
    public static final String MODID = "chocolate";

    //Lets the people looking at the console know what mod is debugging info
    public static String LOGGERMODID(String message) {
        String logModid = ("[" + MODID + "] ");
        return logModid + message;
    }



}
