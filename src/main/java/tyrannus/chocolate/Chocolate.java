package tyrannus.chocolate;

import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRenderers;
import net.minecraft.data.DataGenerator;
import net.minecraft.world.level.block.Block;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.server.ServerAboutToStartEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.InterModEnqueueEvent;
import net.minecraftforge.fml.event.lifecycle.InterModProcessEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.forge.event.lifecycle.GatherDataEvent;
import tyrannus.chocolate.data.client.entity.render.ChocolateSlimeRenderer;
import tyrannus.chocolate.init.world.features.FluidFeature;
import tyrannus.chocolate.init.world.features.OreFeature;
import tyrannus.chocolate.setup.ModBlocks;
import tyrannus.chocolate.setup.ModEntities;
import tyrannus.chocolate.setup.ModFluids;
import tyrannus.chocolate.setup.ModItems;

import static tyrannus.chocolate.ModLogger.*;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(MODID)
public class Chocolate {
    public Chocolate() {


        LOGGER.info(LOGGERMODID("Initializing Chocolate Items"));
        ModItems.init();
        LOGGER.info(LOGGERMODID("Initializing Chocolate Blocks"));
        ModBlocks.init();
        LOGGER.info(LOGGERMODID("Initializing Chocolate Fluids"));
        ModFluids.init();
        LOGGER.info(LOGGERMODID("Initializing Chocolate Entities"));
        ModEntities.init();
        LOGGER.info(LOGGERMODID("Initializing Chocolate Generation"));
        MinecraftForge.EVENT_BUS.addListener(EventPriority.HIGH, OreFeature::onBiomeLoadingEvent);
        //MinecraftForge.EVENT_BUS.addListener(EventPriority.HIGH, FluidGeneration::generateFluids);

        LOGGER.info(LOGGERMODID("Listening"));
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        // Register the enqueueIMC method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::enqueueIMC);
        // Register the processIMC method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::processIMC);
        // Register the doClientStuff method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);


    }

    private void setup(final FMLCommonSetupEvent event) {
        // some pre-init code
        LOGGER.info(LOGGERMODID("The Pre-Choco-inator (Running Pre-Initialization)"));
        event.enqueueWork(() -> {
            ModEntities.getAdditionalAttributes();
            OreFeature.registerOreFeatures();
            //FluidFeature.registerConfiguredFeatures();

        });

    }


    private void doClientStuff(final FMLClientSetupEvent event) {
        // do something that can only be done on the client

        event.enqueueWork(()-> {
            // Block Rendering
            LOGGER.info(LOGGERMODID("Rendering Block Layers"));
            ItemBlockRenderTypes.setRenderLayer(Block.byItem(ModItems.MILK_CHOCOLATE_DOOR.get()), RenderType.cutout());
            ItemBlockRenderTypes.setRenderLayer(Block.byItem(ModItems.MILK_CHOCOLATE_TRAPDOOR.get()), RenderType.cutout());

            // Entity Rendering
            LOGGER.info(LOGGERMODID("Rendering Entity Layers"));
            EntityRenderers.register(ModEntities.CHOCOLATE_SLIME.get(), ChocolateSlimeRenderer::new);

            //Fluid Rendering
            LOGGER.info(LOGGERMODID("Rendering Fluid Layers"));


        });
    }

    private void enqueueIMC(final InterModEnqueueEvent event) {
        // some example code to dispatch IMC to another mod
        /*InterModComms.sendTo("chocolate", "helloworld", () -> {
            LOGGER.info("Hello world from the MDK");
            return "Hello world";
        });*/
    }
    @SubscribeEvent
    public void gatherData(GatherDataEvent event) {
        LOGGER.info(LOGGERMODID("Generating Data"));
        DataGenerator gen = event.getGenerator();
    }
    private void processIMC(final InterModProcessEvent event) {
        // some example code to receive and process InterModComms from other mods
        /*LOGGER.info("Got IMC {}", event.getIMCStream().
                map(m -> m.messageSupplier().get()).
                collect(Collectors.toList()));*/
    }

    // You can use SubscribeEvent and let the Event Bus discover methods to call
    @SubscribeEvent
    public void onServerStarting(ServerAboutToStartEvent event) {
        // do something when the server starts
        LOGGER.info(LOGGERMODID("Starting Chocolate Server Events"));
    }

    // You can use EventBusSubscriber to automatically subscribe events on the contained class (this is subscribing to the MOD
    // Event bus for receiving Registry Events)
    @Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {
        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> blockRegistryEvent) {
            // register a new block here
            LOGGER.info(LOGGERMODID("Chocolate Block Registry"));

        }

    }

}
