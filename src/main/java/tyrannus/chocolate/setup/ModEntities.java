package tyrannus.chocolate.setup;

import net.minecraft.advancements.critereon.EntityTypePredicate;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.RegistryObject;
import tyrannus.chocolate.Chocolate;
import tyrannus.chocolate.init.entities.EntityChocolateSlime;

import static tyrannus.chocolate.ModLogger.MODID;

public class ModEntities {

    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, MODID);

    //Attaches the deferred register to the event bus
    public static void init() {
        ENTITIES.register(FMLJavaModLoadingContext.get().getModEventBus());
    }

    public static final RegistryObject<EntityType<EntityChocolateSlime>> CHOCOLATE_SLIME = ENTITIES.register("chocolate_slime", () ->
            EntityType.Builder.of(EntityChocolateSlime::new, MobCategory.MONSTER)
                    .sized(2.04F, 2.04F)
                    .setTrackingRange(10)
                    .clientTrackingRange(8)
                    .build("chocolate_slime"));
//Slime::checkSlimeSpawnRules
    @SubscribeEvent
    public static void registerEntityAttributes(EntityAttributeCreationEvent event) {
        event.put(ModEntities.CHOCOLATE_SLIME.get(), EntityChocolateSlime.setAttributes());
    }

    public static void getAdditionalAttributes() {
        EntityChocolateSlime.createMobAttributes().build();
    }

}