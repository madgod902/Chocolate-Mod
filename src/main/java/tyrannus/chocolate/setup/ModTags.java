package tyrannus.chocolate.setup;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.*;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.material.Fluid;
import tyrannus.chocolate.Chocolate;

import static tyrannus.chocolate.ModLogger.MODID;

public class ModTags {
    public static final class Blocks {
        public static final TagKey<Block> ORES_CHOCOLATE = forge("ores/chocolate");
        public static final TagKey<Block> STORAGE_BLOCKS = forge("storage_blocks/chocolate");


        private static TagKey<Block> forge(String path) {
            return BlockTags.create(new ResourceLocation("forge", path));
        }

        private static TagKey<Block> mod(String path) {
            return BlockTags.create(new ResourceLocation(MODID, path));
        }
    }

    public static final class Items {
        public static final TagKey<Item> ORES_CHOCOLATE = forge("ores/chocolate");
        public static final TagKey<Item> STORAGE_BLOCKS = forge("storage_blocks/chocolate");

        public static final TagKey<Item> INGOTS_CHOCOLATE = forge("ingots/chocolate");

        private static TagKey<Item> forge(String path) {
            return ItemTags.create(new ResourceLocation("forge", path));
        }
        private static TagKey<Item> mod(String path) {
            return ItemTags.create(new ResourceLocation(MODID, path));
        }
    }

    public static final class Fluids {
        public static final TagKey<Fluid> CHOCOLATE_LIQUIDS = forge("chocolate");

        private static TagKey<Fluid> forge(String path) {
            return FluidTags.create(new ResourceLocation("forge", path));
        }
        private static TagKey<Fluid> mod(String path) {
            return FluidTags.create(new ResourceLocation(MODID, path));
        }
    }

}

